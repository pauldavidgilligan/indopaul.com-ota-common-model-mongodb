/*
 * Copyright (C) 2012 paulgilligan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.indopaul.ota.common.model.mongodb.util;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.springframework.core.io.Resource;

/**
 *
 * @author paulgilligan
 */
public class MongoStationReader {

    private static Logger log = Logger.getLogger(MongoStationReader.class);
    
    private Resource datafile;

    public Resource getDatafile() {
        return datafile;
    }

    public void setDatafile(Resource datafile) {
        this.datafile = datafile;
    }

    public List<MongoStation> read(){
        List<MongoStation> stations = new ArrayList<MongoStation>();        
        BufferedReader br = null;        
        try {
            br = new BufferedReader(new InputStreamReader(this.datafile.getInputStream()));
            String line;
            int row = 0;
            int col = 0;
            while ((line = br.readLine()) != null) {
                StringTokenizer st = new StringTokenizer(line, ",");
                while (st.hasMoreTokens()) {
                    log.info("row " + row + " col " + col + " " + st.nextToken());
                    col++;
                }
                row++;
            }

        } catch (FileNotFoundException fnfe) {
            log.fatal(fnfe);
        } catch (IOException ioe) {
            log.fatal(ioe);
        } finally {
            IOUtils.closeQuietly(br);
        }
        return stations;
    }
}
