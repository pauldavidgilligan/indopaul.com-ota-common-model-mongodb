/*
 * Copyright (C) 2012 paulgilligan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.indopaul.ota.common.model.mongodb.index;

import java.util.Date;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.index.GeoSpatialIndexed;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 *
 * @author paulgilligan
 */
@Document
@CompoundIndexes({
    @CompoundIndex(name = "access_idx", def = "{'accessIndex': 1, 'accessRequest': 1}")
})
public class AccessPointServiceIndex {

    @Id
    private ObjectId id;
    private String accessIndex;
    private String accessRequest;
    
    private String project;
    private String type;
    private Date changed;
    private String changedby;
    private String country;
    private String description;
    private Boolean enabled;
    @GeoSpatialIndexed    
    private double[] location;

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getAccessIndex() {
        return accessIndex;
    }

    public void setAccessIndex(String accessIndex) {
        this.accessIndex = accessIndex;
    }

    public String getAccessRequest() {
        return accessRequest;
    }

    public void setAccessRequest(String accessRequest) {
        this.accessRequest = accessRequest;
    }

    public String getProject() {
        return project;
    }

    public void setProject(String project) {
        this.project = project;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Date getChanged() {
        return changed;
    }

    public void setChanged(Date changed) {
        this.changed = changed;
    }

    public String getChangedby() {
        return changedby;
    }

    public void setChangedby(String changedby) {
        this.changedby = changedby;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public double[] getLocation() {
        return location;
    }

    public void setLocation(double[] location) {
        this.location = location;
    }


    @Override
    public String toString() {
        return "AccessPointServiceIndex{" + "accessIndex=" + accessIndex + ", accessRequest=" + accessRequest + ", project=" + project + ", type=" + type + ", enabled=" + enabled + ", location=" + location + '}';
    }

    
}
