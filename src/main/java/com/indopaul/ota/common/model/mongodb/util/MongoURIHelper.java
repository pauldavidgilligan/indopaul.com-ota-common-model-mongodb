/*
 * Copyright (C) 2012 paulgilligan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.indopaul.ota.common.model.mongodb.util;

import com.mongodb.MongoURI;
import org.apache.log4j.Logger;

/**
 *
 * @author paulgilligan
 */
public class MongoURIHelper extends MongoURI {

    private static Logger log = Logger.getLogger(MongoURIHelper.class);
        
    private String firsthost;
    private int firstport;
    

    public MongoURIHelper(String uri) {
        super(uri);
        
        // just pick any hosts string part, 
        // mongoURI api is a bit weak here 
        // could do with list of URI's
        for(String s: this.getHosts()){
            String[] parts = s.split(":"); 
            this.firsthost = parts[0];
            this.firstport = Integer.parseInt(parts[1]);
        }
        
        log.info("first available parts are : " + this);
    }

    public String getPasswordString() {
        return new String(super.getPassword());
    }
    
    public String getFirstHost(){    
        return this.firsthost;
    }
    
    public int getFirstPort(){    
        return this.firstport;
    }

    @Override
    public String toString() {
        return "parts{" + "firsthost=" + firsthost + ", firstport=" + firstport + '}';
    }
    
    
    
}
