/*
 * Copyright (C) 2012 paulgilligan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.indopaul.ota.common.model.mongodb;

import com.indopaul.ota.common.model.mongodb.operations.IndexOperations;
import org.apache.log4j.Logger;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author paulgilligan
 */
public class Destroy {
    
    private static Logger log = Logger.getLogger(Load.class);    

    public static void main(String[] args){
        log.info("Bootstrapping Mongo OTA Repository");

        ConfigurableApplicationContext context =
                new ClassPathXmlApplicationContext("META-INF/spring/applicationContext.xml");
        
        IndexOperations iops = (IndexOperations) context.getBean("indexOperations");                
        iops.destroy();
        
        log.info("Done!");
    }    
    
}
