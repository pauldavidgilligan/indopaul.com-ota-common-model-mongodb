/*
 * Copyright (C) 2012 paulgilligan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.indopaul.ota.common.model.mongodb.operations;

import com.indopaul.ota.common.model.mongodb.event.IssuerServiceEvent;
import com.indopaul.ota.common.model.mongodb.event.IssuingServiceEvent;
import com.indopaul.ota.common.model.mongodb.event.PersoServiceEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.stereotype.Repository;

/**
 *
 * @author paulgilligan
 */
@Repository
public class EventOperations {

    @Autowired
    MongoOperations mongoOperations;

    public void load() {

        if (mongoOperations.collectionExists(PersoServiceEvent.class)) {
            mongoOperations.dropCollection(PersoServiceEvent.class);
        } else {
            mongoOperations.createCollection(PersoServiceEvent.class);
        }

        if (mongoOperations.collectionExists(IssuerServiceEvent.class)) {
            mongoOperations.dropCollection(IssuerServiceEvent.class);
        } else {
            mongoOperations.createCollection(IssuerServiceEvent.class);
        }

        if (mongoOperations.collectionExists(IssuingServiceEvent.class)) {
            mongoOperations.dropCollection(IssuingServiceEvent.class);
        } else {
            mongoOperations.createCollection(IssuingServiceEvent.class);
        }


    }
}
