/*
 * Copyright (C) 2012 paulgilligan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.indopaul.ota.common.model.mongodb.operations;

import com.indopaul.ota.common.model.mongodb.index.AccessPointServiceIndex;
import com.indopaul.ota.common.model.mongodb.index.CustomerServiceIndex;
import com.indopaul.ota.common.model.mongodb.index.ProjectServiceIndex;
import com.indopaul.ota.common.model.mongodb.util.MongoStationReader;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.stereotype.Repository;

/**
 *
 * @author paulgilligan
 */
@Repository
public class IndexOperations {

    private static Logger log = Logger.getLogger(IndexOperations.class);
    @Autowired
    MongoOperations mongoOperations;
    @Autowired
    MongoStationReader mongoStationReader;

    public void load() {
        log.info("creating repository");

        if(!mongoOperations.collectionExists(CustomerServiceIndex.class)) {
            mongoOperations.createCollection(CustomerServiceIndex.class);
        }

        if(!mongoOperations.collectionExists(ProjectServiceIndex.class)) {
            mongoOperations.createCollection(ProjectServiceIndex.class);
        }

        if(!mongoOperations.collectionExists(AccessPointServiceIndex.class)) {
            mongoOperations.createCollection(AccessPointServiceIndex.class);
        }

        log.info(mongoOperations.getCollectionNames());

        log.info("loading repository data");
        mongoStationReader.read();

    }

    public void destroy() {
        log.info("destroying repository");
        if(mongoOperations.collectionExists(CustomerServiceIndex.class)) {
            mongoOperations.dropCollection(CustomerServiceIndex.class);
        }

        if(mongoOperations.collectionExists(ProjectServiceIndex.class)) {
            mongoOperations.dropCollection(ProjectServiceIndex.class);
        }

        if(mongoOperations.collectionExists(AccessPointServiceIndex.class)) {
            mongoOperations.dropCollection(AccessPointServiceIndex.class);
        }
        
        log.info(mongoOperations.getCollectionNames());
    }
}
