package com.indopaul.ota.common.model.mongodb;

import com.indopaul.ota.common.model.mongodb.operations.IndexOperations;
import org.apache.log4j.Logger;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;


/**
 * Hello world!
 *
 */
public class Load {

    private static Logger log = Logger.getLogger(Load.class);    

    public static void main(String[] args){
        log.info("Bootstrapping Mongo OTA Repository");

        ConfigurableApplicationContext context =
                new ClassPathXmlApplicationContext("META-INF/spring/applicationContext.xml");
        
        IndexOperations iops = (IndexOperations) context.getBean("indexOperations");               
        iops.load();            
        
        log.info("Done!");
    }
}
